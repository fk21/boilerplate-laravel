<?php

use App\Permission_Role;
use Illuminate\Database\Seeder;

class PermissionsRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission_Role::create([
            'role_id'=>1,
            'permission_id'=>1,
            'c'=>1,
            'r'=>1,
            'u'=>1,
            'd'=>1
        ]);
        Permission_Role::create([
            'role_id'=>1,
            'permission_id'=>2,
            'c'=>1,
            'r'=>1,
            'u'=>1,
            'd'=>1
        ]);
        Permission_Role::create([
            'role_id'=>1,
            'permission_id'=>3,
            'c'=>1,
            'r'=>1,
            'u'=>1,
            'd'=>1
        ]);
        Permission_Role::create([
            'role_id'=>1,
            'permission_id'=>4,
            'c'=>1,
            'r'=>1,
            'u'=>1,
            'd'=>1
        ]);
        Permission_Role::create([
            'role_id'=>1,
            'permission_id'=>5,
            'c'=>1,
            'r'=>1,
            'u'=>1,
            'd'=>1
        ]);
        Permission_Role::create([
            'role_id'=>1,
            'permission_id'=>6,
            'c'=>1,
            'r'=>1,
            'u'=>1,
            'd'=>1
        ]);
    }
}
