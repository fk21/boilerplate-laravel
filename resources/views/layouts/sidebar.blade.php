@section('sidebar')
<?php
    function hasChilds($permissions,$id){
        return $permissions['childMenus']->where('parent',$id)->count() > 0;
    }

    function parentMenuClass($parent,$childs){
        if($parent->route == Route::currentRouteName())
            return 'm-menu__item--active';
        return $childs->where('parent',$parent->id)
                    ->where('route',Route::currentRouteName())
                    ->count() > 0 ? 'm-menu__item--open m-menu__item--expanded' : '';
    }
?>

<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
    @foreach($permissions['parentMenus'] as $parent)
    @if($parent->menu)
    <li class="m-menu__item  m-menu__item--submenu {{parentMenuClass($parent,$permissions['childMenus'])}}" aria-haspopup="true" data-menu-submenu-toggle="hover">
        <a href="{{ hasChilds($permissions,$parent->id) ? 'javascript:void();' : route($parent->route) }}" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon {{$parent->icon}}"></i>
            <span class="m-menu__link-text">
                {{$parent->title}}
            </span>
            @if(hasChilds($permissions,$parent->id))
            <i class="m-menu__ver-arrow la la-angle-right"></i>
            @endif
        </a>
        @if(hasChilds($permissions,$parent->id))
        <div class="m-menu__submenu">
            <span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
                @foreach ($permissions['childMenus']->where('parent',$parent->id) as $child)
                <li class="m-menu__item {{ $child->route == Route::currentRouteName() ?  'm-menu__item--active' : '' }}" aria-haspopup="true">
                    <a href="{{ route($child->route)}}" class="m-menu__link ">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                            <span></span>
                        </i>
                        <span class="m-menu__link-text">
                            {{$child->title}}
                        </span>
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        @endif
    </li>
    @else
    <li class="m-menu__section">
        <h4 class="m-menu__section-text">
            {{$parent->title}}
        </h4>
        <i class="m-menu__section-icon flaticon-more-v3"></i>
    </li>
    @endif
    @endforeach
</ul>
@stop