<?php

namespace App\Http\ViewComposers;

use Auth;
use App\User;
use App\Permission_Role;
use Illuminate\View\View;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class PermissionsComposer
{
    /*
    * Create a movie composer.
    *
    * @return void
    */
    protected $crud;

    public function __construct()
    {
        $permissions = Permission_Role::
        with(['permission'=> function ($q)
        {
            $q->where('route',Route::currentRouteName());
        }])
        ->where('role_id',Auth::user()->role_id)
        ->get();
        $this->crud = $permissions->where('permission','!=',null)->first();  
    }
    
    /**
    * Bind data to the view.
    *
    * @param  View  $view
    * @return void
    */
    public function compose(View $view)
    {
        $view->with('crud', $this->crud);
    }
}