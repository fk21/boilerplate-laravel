<?php

namespace App;

use App\User;
use App\Permission;
use App\Permission_Role;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'description'
    ];

    public function permissions(){
        return $this->belongsToMany('App\Permission', 'permission__roles')->withPivot('C', 'R', 'U', 'D');
    }
}
